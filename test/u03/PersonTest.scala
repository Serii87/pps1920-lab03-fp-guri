package u03
import org.junit.jupiter.api.Assertions._
import org.junit.jupiter.api.Test
import u02.Modules

import u02.Modules.Person
import u03.Lists.List.Cons

class PersonTest {

  import Lists.List._
  import Person._
  val persons= Cons[Person](Teacher("matteo","reti"), Cons(Student("mario",2015), Cons(Student("sokol",2015), Cons(Teacher("ricco", "web"), Nil()))))

  @Test def getCoursesOfTest(): Unit ={
    assertEquals(Cons("reti", Cons("web", Nil())), getCoursesFrom(persons))
    assertEquals(Cons("math", Cons("C++", Cons("reti", Cons("web",  Nil())))), getCoursesFrom(append[Person](Cons(Teacher("pitter", "math"), Cons(Teacher("remo", "C++"), Nil())), persons)))
    assertEquals(Cons("web", Cons("reti", Cons("web",  Nil()))), getCoursesFrom(append(Cons(Teacher("mister", "web"), Nil()), persons)))
  }

}
