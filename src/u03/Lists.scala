package u03

import u02.Optionals

object Lists {

  // A generic linkedlist
  sealed trait List[E]

  // a companion object (i.e., module) for List
  object List {
    case class Cons[E](head: E, tail: List[E]) extends List[E]
    case class Nil[E]() extends List[E]

    def sum(l: List[Int]): Int = l match {
      case Cons(h, t) => h + sum(t)
      case _ => 0
    }

    def append[A](l1: List[A], l2: List[A]): List[A] = (l1, l2) match {
      case (Cons(h, t), l2) => Cons(h, append(t, l2))
      case _ => l2
    }

    def map[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(h, t) => Cons(mapper(h), map(t)(mapper))
      case Nil() => Nil()
    }

    def filter[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(h,t) if (pred(h)) => Cons(h, filter(t)(pred))
      case Cons(_,t) => filter(t)(pred)
      case Nil() => Nil()
    }

    def drop[A](l: List[A], n: Int): List[A] = l match {
      case Cons(_, tail) if(n>0) => drop(tail, n-1)
      case _ => l
    }

    def flatMap[A,B](l: List[A])(f: A=>List[B]): List[B] = l match {
      case Cons(head, tail) => append(f(head), flatMap(tail)(f))
      case _ => Nil()
    }

    def mapWithFlatMap[A,B](l: List[A])(mapper: A=>B): List[B] = l match {
      case Cons(head, tail) => flatMap(l)(head => Cons(mapper(head), Nil()))
      case _ => Nil()
    }

    def filterWithFlatMap[A](l1: List[A])(pred: A=>Boolean): List[A] = l1 match {
      case Cons(head, tail) if(pred(head)) => flatMap(l1)(head => Cons(head, Nil()))
      case Cons(_, tail) => filterWithFlatMap(tail)(pred)
      case _ => Nil()
    }

  import u02.Optionals._
    def max(l: List[Int]): Option[Int] =   l match {
      case Nil() => Option.None()
      case Cons(head, tail) =>
        @annotation.tailrec
        def compareMaxElem(list: List[Int], max: Int): Option[Int] = list match {
          case Cons(h,t)  if(h>=max) => compareMaxElem(t, h)
          case Cons(h,t) => compareMaxElem(t, max)
          case _ => Option.Some[Int](max)
        }
        compareMaxElem(l, head)
    }
  }

}


